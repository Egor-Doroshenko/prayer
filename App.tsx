import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { Column } from './src/components/Column/Column';
import { Desc } from './src/components/Desc/Desc';
import { Header } from './src/components/Header/Header';
import { Navbar } from './src/components/Navbar/Navbar';
import { DetailsNavbar } from './src/components/TaskDetails/DetailsNavbar/DetailsNavbar';
import { TaskDetails } from './src/components/TaskDetails/TaskDetails';
import { ITask } from './src/interfaces';

const tasks: ITask[] = [
  { title: 'task name 1', prayers: 5, prayed: 123 },
  { title: 'task name 2', prayers: 3, prayed: 5 },
  { title: 'task name 3', prayers: 8, prayed: 345 },
  { title: 'task name 4', prayers: 13, prayed: 11 },
]

const Stack = createNativeStackNavigator();



const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Desc"
          options={{ headerTitle: props => <Header title="My desc" /> }}
        >
          {props => <Desc descName="My desc" />}
        </Stack.Screen>
        <Stack.Screen
          name="Column"
          options={{ headerTitle: props => <Navbar title="To do" /> }}
        >
          {props => <Column title="To do" tasks={tasks} />}
        </Stack.Screen>
        <Stack.Screen
          name="TaskDetails"
          options={{
            headerTitle: props => <DetailsNavbar title="Prayer item two which is for my family to love God whole heartedly." />,
            headerStyle: {
              backgroundColor: '#BFB393'
            }
          }}
        >
          {props => <TaskDetails
            title="Prayer item two which is for my family to love God whole heartedly."
            lastPrayed="Last prayed 8 mins ago"
            dateAdded="July 25 2017"
            prayed={123}
            prayedByMe={120}
            prayedByOthers={3}
          />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;