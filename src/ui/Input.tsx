import React from "react";
import { CSSProp } from "styled-components";
import styled from "styled-components/native";

export const Input: React.FC<InputProps> = ({ placeholder, css }: InputProps) => {
    return (
        <StyledTextInput placeholder={placeholder} $CSS={css} />
    )
}

interface InputProps {
    placeholder?: string,
    css?: CSSProp,
}
Input.defaultProps = {
    placeholder: 'Type here...',
    css: {}
}
interface StyledTextInputProps {
    $CSS?: CSSProp,
}

const StyledTextInput = styled.TextInput<StyledTextInputProps>`
    background-color: #fff;
    border-width: 0px;
    font-family: SFUIText-Regular;
    font-size: 17px;
    line-height: 20px;
    ${({ $CSS }) => $CSS}
`