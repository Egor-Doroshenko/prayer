import React from "react";
import { CSSProp } from "styled-components";
import styled from "styled-components/native";

export const Container: React.FC<ContainerProps> = ({ children, css }: ContainerProps) => {
    return (<StyledView $CSS={css}>
        {children}
    </StyledView>)

}

interface ContainerProps {
    children?: JSX.Element[] | JSX.Element,
    css: CSSProp,
}
Container.defaultProps = {
    children: [<></>]
}
interface StyledViewProps {
    $CSS?: CSSProp,
}


const StyledView = styled.View<StyledViewProps>`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  height: auto;
  background-color: #fff;
  ${({ $CSS }) => $CSS}
`
StyledView.defaultProps = {
    $CSS: {},
}