import React from "react";
import { CSSProp } from "styled-components";
import styled from "styled-components/native";

export const IconWrapper: React.FC<IconWrapperProps> = ({ children, css }: IconWrapperProps) => {
    return (<StyledView $CSS={css}>
        {children}
    </StyledView>)

}

interface IconWrapperProps {
    children?: JSX.Element,
    css?: CSSProp,
}
IconWrapper.defaultProps = {
    children: <></>
}
interface StyledViewProps {
    $CSS?: CSSProp,
}


const StyledView = styled.View<StyledViewProps>`
    width: auto;
    height: auto;
    ${({ $CSS }) => $CSS}
`
StyledView.defaultProps = {
    $CSS: {},
}