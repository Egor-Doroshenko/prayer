import React from "react"
import { CSSProp } from "styled-components"
import styled from "styled-components/native"
import { css } from "styled-components"
import { Txt } from "./Text"

export const Btn: React.FC<TouchibleOpacityProps> = ({ children, title, onPress, css }: TouchibleOpacityProps) => {
    if (title) {
        return (
            <StyledTouchibleOpacity onPress={onPress} $CSS={css}>
                <Txt css={CSS.btnTitle} text={title} />
            </StyledTouchibleOpacity>
        )
    }
    return (
        <StyledTouchibleOpacity onPress={onPress} $CSS={css}>
            {children}
        </StyledTouchibleOpacity>
    )
}

interface TouchibleOpacityProps {
    title?: string,
    onPress: () => void,
    css?: CSSProp,
    children?: JSX.Element,
}
interface StyledTouchibleOpacityProps {
    $CSS?: CSSProp,
}


const StyledTouchibleOpacity = styled.TouchableOpacity<StyledTouchibleOpacityProps>`
    border-radius: 15px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 17px;
    padding-right: 17px;
    ${({ $CSS }) => $CSS}
`
const CSS = {
    btnTitle: css`
        font-family: SF UI Text;
        font-size: 12px;
        line-height: 14px;
        text-align: center;
        text-transform: uppercase;
        color: #fff;
    `
}