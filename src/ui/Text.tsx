import React from "react";
import styled from "styled-components/native";
import { CSSProp } from "styled-components";

export const Txt: React.FC<TxtProps> = ({ text, css }: TxtProps) => {
    return (
        <StyledText $CSS={css}>
            {text}
        </StyledText>
    )
}

interface TxtProps {
    text: string,
    css?: CSSProp,
}
Txt.defaultProps = {
    css: {}
}
interface StyledTextProps {
    $CSS?: CSSProp,
}

const StyledText = styled.Text<StyledTextProps>`
    font-family: SFUIText-Regular;
    font-size: 17px;
    line-height: 20px;
    color: #514D47;
    ${({ $CSS }) => $CSS}
`