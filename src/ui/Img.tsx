import React from "react";
import { CSSProp } from "styled-components";
import styled from "styled-components/native";

export const Img: React.FC<ImgProps> = ({ src, css }: ImgProps) => {
    return (
        <StyledImage source={{ uri: src }} $CSS={css} />
    )
}

interface ImgProps {
    src: string,
    css?: CSSProp
}
interface StyledImageProps {
    $CSS?: CSSProp
}

const StyledImage = styled.Image<StyledImageProps>`
    width: 100%;
    height: auto;
    ${({ $CSS }) => $CSS}
`;