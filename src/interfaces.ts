export interface IColumn {
    title: string,
    id: string,
}

export interface ITask {
    title: string,
    prayers: number,
    prayed: number
}