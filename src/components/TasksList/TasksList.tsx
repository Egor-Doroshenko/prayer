import React from "react";
import { css } from "styled-components";
import { ITask } from "../../interfaces";
import { Container } from "../../ui/Container";
import { Task } from "../Task/Task";

interface TasksListProps {
    tasks: ITask[]
}

export const TasksList: React.FC<TasksListProps> = ({ tasks }: TasksListProps) => {
    return (
        <Container css={CSS.wrapper}>
            {tasks.map((task) => {
                return (
                    <Task
                        title={task.title}
                        prayers={task.prayers}
                        prayed={task.prayed}
                    />
                )
            })}
        </Container>
    )
}

const CSS = {
    wrapper: css`
        
    `
}