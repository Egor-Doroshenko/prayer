import React from "react";
import { IconWrapper } from "../../ui/IconWrapper";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { css } from "styled-components";
import { Container } from "../../ui/Container";
import { Input } from "../../ui/Input";

export const AddTaskForm: React.FC = () => {
    return (
        <Container css={CSS.container}>
            <IconWrapper css={CSS.icon}>
                <FontAwesome5 name={'plus'} color={'#72A8BC'} size={16} />
            </IconWrapper>
            <Input css={CSS.input} placeholder="Add a prayer..." />
        </Container>
    )
}

const CSS = {
    container: css`
        margin-left: 15px;
        margin-right: 15px;
        margin-top: 10px;
        height: 50px;
        flex-direction: row;
        border-width: 1px;
        border-color: #E5E5E5;
        border-radius: 10px;
    `,
    icon: css`
        margin-left: 15px;
        margin-right: 15px;
    `,
    input: css`
        flex-grow: 1;
        border-radius: 11px;
    `
}