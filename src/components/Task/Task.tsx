import React from "react";
import { css } from "styled-components";
import { Container } from "../../ui/Container";
import CheckBox from '@react-native-community/checkbox';
import { Txt } from "../../ui/Text";
import { IconWrapper } from "../../ui/IconWrapper";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Btn } from "../../ui/Btn";
import { useNavigation } from "@react-navigation/native";

interface TaskProps {
    title: string,
    prayers: number,
    prayed: number
}

export const Task: React.FC<TaskProps> = ({ title, prayers, prayed }: TaskProps) => {
    const navigation = useNavigation();
    const navigateToTaskDetails = () => {
        navigation.navigate('TaskDetails');
    }

    return (
        <Container css={CSS.container}>
            <Container css={CSS.left}>
                <Container css={CSS.line}></Container>
                <CheckBox />
                <Btn onPress={navigateToTaskDetails}>
                    <Txt text={title} css={CSS.text} />
                </Btn>
            </Container>
            <Container css={CSS.right}>
                <IconWrapper css={CSS.icon}>
                    <FontAwesome5 name={'user'} color={'#72A8BC'} size={16} />
                </IconWrapper>
                <Txt text={prayers.toString()} />
                <IconWrapper css={CSS.icon}>
                    <FontAwesome5 name={'praying-hands'} color={'#72A8BC'} size={16} />
                </IconWrapper>
                <Txt text={prayed.toString()} />
            </Container>
        </Container>
    )
}

const CSS = {
    container: css`
        width: 100%;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        padding-bottom: 15px;
        padding-top: 15px;
        border-bottom-width: 1px;
        border-bottom-color: #E5E5E5;
    `,
    left: css`
        max-width: 50%;
        flex-direction: row;
        align-items: center;
        justify-content: flex-start;
    `,
    right: css`
        max-width: 50%;
        flex-direction: row;
        align-items: center;
        justify-content: flex-end;
    `,
    line: css`
        width: 0px;
        height: 24px;
        border-width: 1.5px;
        border-color: #AC5253;
        border-radius: 3px;
    `,
    icon: css`
        margin-left: 15px;
        margin-right: 15px;
    `,
    text: css`
        max-width: 200px;
    `
}