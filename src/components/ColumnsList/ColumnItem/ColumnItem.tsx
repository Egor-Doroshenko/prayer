import { useNavigation } from "@react-navigation/native";
import React from "react";
import { css } from "styled-components";
import { Btn } from "../../../ui/Btn";
import { Container } from "../../../ui/Container";
import { Txt } from "../../../ui/Text";

interface ColumnItemProps {
    title: string,
}

export const ColumnItem: React.FC<ColumnItemProps> = ({ title }: ColumnItemProps) => {
    const navigation = useNavigation();
    const navigateToColumn = () => {
        navigation.navigate('Column');
    }

    return (
        <Btn css={CSS.btn} onPress={navigateToColumn}>
            <Container css={CSS.container}>
                <Txt text={title} />
            </Container>
        </Btn>
    )
};

const CSS = {
    container: css`
        padding-left: 15px;
        padding-top: 20px;
        padding-bottom: 20px;
        border-width: 1px;
        border-color: #E5E5E5;
        border-radius: 4px;
        margin-bottom: 10px;
        align-items: flex-start;
    `,
    btn: css`
        width: 100%;
        padding-left: 0;
        padding-right: 0;
    `
}