import React from "react";
import { css } from "styled-components";
import { IColumn } from "../../interfaces";
import { Container } from "../../ui/Container";
import { ColumnItem } from "./ColumnItem/ColumnItem";

interface ColumnsListProps {
    columns: IColumn[],
}

export const ColumnsList: React.FC<ColumnsListProps> = ({ columns }: ColumnsListProps) => {
    return (
        <Container css={CSS.container}>
            {columns.map((column) => {
                return (
                    <ColumnItem title={column.title} />
                )
            })}
        </Container>
    )
}

const CSS = {
    container: css`
        padding-left: 15px;
        padding-right: 15px;
        padding-top: 15px;
    `,

}