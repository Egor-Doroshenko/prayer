import React from "react";
import { Alert } from "react-native";
import { css } from "styled-components";
import { ITask } from "../../../interfaces";
import { Btn } from "../../../ui/Btn";
import { Container } from "../../../ui/Container";
import { AddTaskForm } from "../../AddTaskForm/AddTaskForm";
import { TasksList } from "../../TasksList/TasksList";

interface MyPrayersProps {
    tasks: ITask[],
}

export const MyPrayers: React.FC<MyPrayersProps> = ({ tasks }: MyPrayersProps) => {
    return (
        <Container css={CSS.wrapper}>
            <AddTaskForm />
            <TasksList tasks={tasks} />
            <Btn title="Show answered prays" css={CSS.btn} onPress={() => { Alert.alert('Show ansvered prays') }} />
        </Container>
    )
}

const CSS = {
    wrapper: css`
        padding-left: 15px;
        padding-right: 15px;
        height: 100%;
    `,
    btn: css`
        background-color: #BFB393;
        margin-top: 20px;
    `
}