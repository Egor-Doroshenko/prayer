import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import React from "react";
import { css } from "styled-components";
import { ITask } from "../../interfaces";
import { Container } from "../../ui/Container";
import { MyPrayers } from "./MyPrayers/MyPrayers";
import { Subscribed } from "./Subscribed/Subscribed";

interface ColumnProps {
    title: string,
    tasks: ITask[]
}

const Tab = createMaterialTopTabNavigator();

export const Column: React.FC<ColumnProps> = ({ title, tasks }: ColumnProps) => {
    return (
        <Tab.Navigator>
            <Tab.Screen name="My prayers">
                {props => <MyPrayers tasks={tasks} />}
            </Tab.Screen>
            <Tab.Screen name="Subscribed">
                {props => <Subscribed tasks={tasks} />}
            </Tab.Screen>
        </Tab.Navigator>
    )
}

const CSS = {
    wrapper: css`
        width: 100%;
        padding-left: 15px;
        padding-right: 15px;
        padding-top: 10px;
        padding-bottom: 10px;
        height: 100%;
        border-top-width: 1px;
        border-top-color: #E5E5E5;
    `
}