import React from "react";
import { Header } from "../Header/Header";

// trash
import { IColumn } from '../../interfaces';
import { ColumnsList } from "../ColumnsList/ColumnsList";
import { Container } from "../../ui/Container";
import { css } from "styled-components";
const columns: IColumn[] = [{ title: 'To do', id: '12' }, { title: 'To do', id: '12' }];

interface DescProps {
    descName: string,
}

export const Desc: React.FC<DescProps> = ({ descName }: DescProps, { navigation }) => {
    return (
        <>
            <Container css={CSS.wrapper}>
                <ColumnsList columns={columns} />
            </Container>

        </>
    )
}

const CSS = {
    wrapper: css`
        height: 100%;
        border-top-width: 1px;
        border-top-color: #E5E5E5;
    `
}