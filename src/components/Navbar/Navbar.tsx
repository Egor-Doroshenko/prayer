import React from "react";
import { Container } from "../../ui/Container";
import { Txt } from "../../ui/Text";
import { IconWrapper } from "../../ui/IconWrapper";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { css } from "styled-components";

interface NavbarProps {
    title: string
}

export const Navbar: React.FC<NavbarProps> = ({ title }: NavbarProps) => {
    return (
        <Container css={CSS.wrapper}>
            <IconWrapper>
                <FontAwesome5 name={'arrow-left'} color={'#72A8BC'} size={16} />
            </IconWrapper>
            <Txt text={title} />
            <IconWrapper>
                <FontAwesome5 name={'cog'} color={'#72A8BC'} size={16} />
            </IconWrapper>
            {/* <Container css={CSS.tabsWrapper}>
                <Container css={CSS.tab}>
                    <Txt text="My prayers" css={CSS.tabTitle} />
                </Container>
                <Container css={CSS.tab}>
                    <Txt text="Subscribed" css={CSS.tabTitle} />
                    <Container css={CSS.round} >
                        <Txt text="4" css={CSS.roundNumber} />
                    </Container>
                </Container>

            </Container> */}
        </Container>
    )
}

const CSS = {
    wrapper: css`
        position: relative;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: space-between;
        align-items: flex-start;
        padding-top: 15px;
        padding-bottom: 15px;
        position: relative;
        margin-left: -18%;
        margin-right: 15%;
    `,
    tabsWrapper: css`
        flex-direction: row;

    `,
    tab: css`
        width: 50%;
        padding-top: 20px;
        flex-direction: row;
        justify-content: center;
    `,
    tabTitle: css`
        font-size: 13px;
        line-height: 15px;
        text-transform: uppercase;
        color: #C8C8C8;
    `,
    round: css`
        min-width: 16px;
        width: auto;
        height: 16px;
        border-radius: 8px;
        background-color: #AC5253;
        margin-left: 4px;
        justify-content: center;
    `,
    roundNumber: css`
        font-size: 9px;
        line-height: 11px;
        color: #FFFFFF;
    `

}