import React from "react";
import { Image } from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { css } from "styled-components";
import { Container } from "../../ui/Container";
import { IconWrapper } from "../../ui/IconWrapper";
import { Img } from "../../ui/Img";
import { Txt } from "../../ui/Text";
import { DetailsNavbar } from "./DetailsNavbar/DetailsNavbar";

interface TaskDetailsProps {
    title: string,
    lastPrayed: string,
    dateAdded: string,
    prayed: number,
    prayedByMe: number,
    prayedByOthers: number,
}


export const TaskDetails: React.FC<TaskDetailsProps> = (
    {
        title,
        lastPrayed,
        dateAdded,
        prayed,
        prayedByMe,
        prayedByOthers
    }: TaskDetailsProps
) => {
    return (
        <>
            <Container css={CSS.wrapper}>
                <Container css={CSS.lastPrayedContainer}>
                    <Container css={CSS.line}></Container>
                    <Txt text={lastPrayed} />
                </Container>
                <Container css={CSS.infoWrapper}>
                    <Container css={CSS.infoContainer}>
                        <Txt text="July 25 2017" css={CSS.infoDate} />
                        <Txt text="Date Added" css={CSS.infoNote} />
                        <Txt text="Opened for 4 days" css={CSS.infoOpened} />
                    </Container>
                    <Container css={CSS.infoContainer}>
                        <Txt text={prayed.toString()} css={CSS.infoTitle} />
                        <Txt text="Times prayed total" css={CSS.infoNote} />
                    </Container>
                    <Container css={CSS.infoContainer}>
                        <Txt text={prayedByMe.toString()} css={CSS.infoTitle} />
                        <Txt text="Times Prayed by me" css={CSS.infoNote} />
                    </Container>
                    <Container css={CSS.infoContainer}>
                        <Txt text={prayedByOthers.toString()} css={CSS.infoTitle} />
                        <Txt text="Times prayed by others" css={CSS.infoNote} />
                    </Container>
                </Container>
                <Container css={CSS.membersWrapper}>
                    <Txt text='Members' css={CSS.title} />
                    <Container css={CSS.membersImgs}>
                        <Img src="https://timesofindia.indiatimes.com/photo/67586673.cms" css={CSS.memberImg} />
                        <Img src="https://c.files.bbci.co.uk/12A9B/production/_111434467_gettyimages-1143489763.jpg" css={CSS.memberImg} />
                        <Img src="https://i.natgeofe.com/n/f0dccaca-174b-48a5-b944-9bcddf913645/01-cat-questions-nationalgeographic_1228126.jpg" css={CSS.memberImg} />
                        <Container css={CSS.round}>
                            <IconWrapper>
                                <FontAwesome5 name={'plus'} color={'#fff'} size={18} />
                            </IconWrapper>
                        </Container>
                    </Container>
                </Container>
                <Container css={CSS.commentsWrapper}>
                    <Txt text='Comments' css={CSS.commentsTitle} />
                    <Container css={CSS.commentWrapper}>
                        <Img src="https://timesofindia.indiatimes.com/photo/67586673.cms" css={CSS.commentImg} />
                        <Container css={CSS.commentTextWrapper}>
                            <Container css={CSS.commentInfoWrapper}>
                                <Txt text="Anna Barber" css={CSS.commentAuthor} />
                                <Txt text="2 days ago" css={CSS.commentDaysAgo} />
                            </Container>
                            <Txt text="Hey!" css={CSS.commentText} />
                        </Container>
                    </Container>
                </Container>
            </Container>
        </>
    )
}

const CSS = {
    wrapper: css`
        height: 100%;
        `,
    lastPrayedContainer: css`
        padding-left: 15px;
        padding-top: 15px;
        flex-direction: row;
        padding-bottom: 15px;
        `,
    line: css`
        width: 0px;
        height: 24px;
        border-width: 2px;
        border-color: #AC5253;
        border-radius: 3px;
        margin-right: 10px;
        `,
    infoWrapper: css`
        padding: 0;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
        align-items: flex-start;
        border-top-width: 0.5px;
        border-bottom-width: 0.5px;
        border-color: #E5E5E5;
        `,
    infoContainer: css`
        width: 50%;
        align-items: flex-start;
        justify-content: flex-start;
        padding-left: 15px;
        padding-top: 26px;
        border-left-width: 0.5px;
        border-right-width: 0.5px;
        border-top-width: 0.5px;
        border-bottom-width: 0.5px;
        border-color: #E5E5E5;
        height: 108px;
        `,
    infoDate: css`
        font-size: 22px;
        line-height: 37px;
        color: #BFB393;
        `,
    infoTitle: css`
        font-size: 32px;
        line-height: 37px;
        color: #BFB393;
        `,
    infoNote: css`
        font-size: 13px;
        line-height: 15px;
        color: #514D47;
        `,
    infoOpened: css`
        font-size: 13px;
        line-height: 15px;
        color: #72A8BC;

        `,
    membersWrapper: css`
        align-items: flex-start;
        padding-left: 15px;
        padding-top: 20px;
        margin-bottom: 30px;
        `,
    title: css`
        font-size: 13px;
        line-height: 15px;
        text-transform: uppercase;
        color: #72A8BC;
        `,
    membersImgs: css`
        flex-direction: row;
        flex-wrap: wrap;
        margin-top: 15px;
    `,
    memberImg: css`
        height: 36px;
        width: 36px;
        border-radius: 18px;
        margin-right: 5px;
    `,
    round: css`
        width: 36px;
        height: 36px;
        border-radius: 18px;
        background-color: #BFB393;
        justify-content: center;
    `,
    commentsWrapper: css`
        align-items: flex-start;
        padding-top: 20px;
        margin-bottom: 30px;
        `,
    commentsTitle: css`
        font-size: 13px;
        line-height: 15px;
        text-transform: uppercase;
        color: #72A8BC;
        margin-left: 15px;
        margin-bottom: 30px;
    `,
    commentWrapper: css`
        border-top-width: 1px;
        border-top-color: #e5e5e5;
        flex-direction: row;
        padding-left: 15px;
        padding-top: 15px;
    `,
    commentTextWrapper: css`
        align-items: flex-start;
    `,
    commentInfoWrapper: css`
        flex-direction: row;
        margin-bottom: 3px;
    `,
    commentImg: css`
        height: 46px;
        width: 46px;
        border-radius: 23px;
        margin-right: 10px;
    `,
    commentAuthor: css`
        font-size: 17px;
        line-height: 20px;
        color: #514D47;
        margin-right: 6px;
    `,
    commentDaysAgo: css`
        font-size: 13px;
        line-height: 16px;
        color: #9C9C9C;
    `,
    commentText: css`
        font-size: 17px;
        line-height: 20px;
        color: #514D47;
    `
}