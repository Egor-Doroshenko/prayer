import React from "react";
import { css } from "styled-components";
import { Btn } from "../../../ui/Btn";
import { Container } from "../../../ui/Container";

import { IconWrapper } from "../../../ui/IconWrapper";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Alert } from "react-native";
import { Txt } from "../../../ui/Text";

interface DetailsNavbarProps {
    title: string
}

export const DetailsNavbar: React.FC<DetailsNavbarProps> = ({ title }: DetailsNavbarProps) => {
    return (
        <Container css={CSS.wrapper}>
            <Container css={CSS.navigation}>
                <Btn title='' css={CSS.btn} onPress={() => { Alert.alert('Show ansvered prays') }}>
                    <IconWrapper>
                        <FontAwesome5 name={'arrow-left'} color={'#ffffff'} size={16} />
                    </IconWrapper>
                </Btn>
                <Btn title='' css={CSS.btn} onPress={() => { Alert.alert('Show ansvered prays') }}>
                    <IconWrapper>
                        <FontAwesome5 name={'praying-hands'} color={'#ffffff'} size={16} />
                    </IconWrapper>
                </Btn>
            </Container>
            <Txt text={title} css={CSS.title} />
        </Container>
    )
}

const CSS = {
    wrapper: css`
        background-color: #BFB393;
        margin-left: -18%;
        margin-right: 15%;
    `,
    navigation: css`
        background-color: #BFB393;
        flex-direction: row;
        justify-content: space-between;
    `,
    btn: css`
        margin-top: 15px;
        margin-bottom: 15px;
        padding-left: 0;
        padding-right: 0;
    `,
    title: css`
        color: #fff;
        margin-bottom: 25px;
    `
}