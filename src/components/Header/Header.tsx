import React from "react";
import { css } from "styled-components";
import { Container } from "../../ui/Container";
import { Txt } from "../../ui/Text";
import { IconWrapper } from "../../ui/IconWrapper";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

interface HeaderProps {
    title: string,
}

export const Header: React.FC<HeaderProps> = ({ title }: HeaderProps) => {
    return (
        <Container css={CSS.container}>
            <Txt text={title} />
            <IconWrapper css={CSS.icon}>
                <FontAwesome5 name={'plus'} color={'#72A8BC'} size={16} />
            </IconWrapper>
        </Container>
    )
}

const CSS = {
    container: css`
        height: 65px;
        align-items: center;
        justify-content: center;
        position: relative;
        margin-left: -3%;
    `,
    icon: `
        position: absolute;
        right: 4px;
        top: 22px;
    `,
}